package com.pavelchak.service;

import com.pavelchak.DAO.implementation.DepartmentDaoImpl;
import com.pavelchak.DAO.implementation.MetaDataDaoImpl;
import com.pavelchak.model.metadata.TableMetaData;

import java.sql.SQLException;
import java.util.List;

public class MetaDataService {
    public List<String> findAllTableName() throws SQLException {
        return new MetaDataDaoImpl().findAllTableName();
    }

    public List<TableMetaData> getTablesStructure() throws SQLException {
        return new MetaDataDaoImpl().getTablesStructure();
    }

}
