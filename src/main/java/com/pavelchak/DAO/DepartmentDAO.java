package com.pavelchak.DAO;

import com.pavelchak.model.DepartmentEntity;

import java.sql.SQLException;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


