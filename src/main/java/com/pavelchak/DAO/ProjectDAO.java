package com.pavelchak.DAO;

import com.pavelchak.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
