package com.pavelchak.DAO;

import com.pavelchak.model.PK_WorksOn;
import com.pavelchak.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
